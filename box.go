package grunigram

import (
	"strings"
)

type Alignment int

const (
	Start Alignment = iota
	Middle
	End
)

type Box struct {
	content []string
	minWidth int
}

func NewBox(value string) *Box {
	lines := strings.Split(strings.ReplaceAll(value, "\r", ""), "\n")
	minWidth := 0
	for i := 0; i < len(lines); i++ {
		minWidth = max(minWidth, len([]rune(lines[i])))
	}
	return &Box{content: lines, minWidth: minWidth}
}

func (b *Box) HorizAlign(al Alignment) {
	for i := 0; i < len(b.content); i++ {
		b.content[i] = strings.TrimSpace(b.content[i])
		fs := b.minWidth - len([]rune(b.content[i]))
		ls, rs := 0, 0
		switch al {
		case Start: rs = fs
		case Middle: ls, rs = (fs+1)/2, fs/2 
		case End: ls = fs
		}
		b.content[i] = strings.Repeat(" ", ls) + b.content[i] + strings.Repeat(" ", rs)
	}
	b.horizPadding()
}

func (b *Box) String() string {
	var sb strings.Builder
	line := strings.Repeat("─", b.minWidth + 2)
	sb.WriteString("╭" + line + "╮\n")
	for i := 0; i < len(b.content); i++ {
		sb.WriteString("│" + b.content[i] + "│\n")
	}
	sb.WriteString("╰" + line + "╯")
	return sb.String()
}

func (b *Box) horizPadding() {
	for i := 0; i < len(b.content); i++ {
		b.content[i] = " " + b.content[i] + " "
	}
}