package grunigram_test

import (
	"fmt"
	"testing"

	"gitlab.com/diegorosalio19/grunigram"
)

func Test(t *testing.T) {
	testCases := []struct {
		content	string
		align grunigram.Alignment
		expected string
	}{
		{
			content: "johnweakness",
			align: grunigram.Start,
			expected:
`╭──────────────╮
│ johnweakness │
╰──────────────╯`,
		},
		{
			content: "john¤\n\rweak\rness\r",
			align: grunigram.Start,
			expected:
`╭──────────╮
│ john¤    │
│ weakness │
╰──────────╯`,
		},
		{
			content: "john\n\nweakness",
			align: grunigram.Start,
			expected:
`╭──────────╮
│ john     │
│          │
│ weakness │
╰──────────╯`,
		},
		{
			content: "john\n\nweakness",
			align: grunigram.End,
			expected:
`╭──────────╮
│     john │
│          │
│ weakness │
╰──────────╯`,
		},
		{
			content: "john\n\nweakness",
			align: grunigram.Middle,
			expected:
`╭──────────╮
│   john   │
│          │
│ weakness │
╰──────────╯`,
		},
		{
			content: "johnn\nweakness",
			align: grunigram.Middle,
			expected:
`╭──────────╮
│   johnn  │
│ weakness │
╰──────────╯`,
		},
	}
	for i, tC := range testCases {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			b := grunigram.NewBox(tC.content)
			b.HorizAlign(tC.align)
			if got := b.String(); got != tC.expected {
				t.Errorf("\nExpected:\n%s\nGot:\n%s\n", tC.expected, got)					
			}
		})
	}
}