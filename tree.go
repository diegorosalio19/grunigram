package grunigram

type Node struct {
	Value string
	Children []Node
}